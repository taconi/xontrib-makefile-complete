# Xontrib Makefile Complete

[![License: GPL v3](https://img.shields.io/pypi/l/xontrib-makefile-complete?label=License&color=%234B78E6)](https://codeberg.org/taconi/xontrib-makefile-complete/raw/branch/main/LICENSE)
[![PyPI version](https://img.shields.io/pypi/v/xontrib-makefile-complete.svg?logo=pypi&label=PyPI&color=%23FA9BFA)](https://pypi.org/project/xontrib-makefile-complete/)
[![Supported Python versions](https://img.shields.io/pypi/pyversions/xontrib-makefile-complete.svg?logo=python&label=Python&color=%234B78E6)](https://pypi.python.org/pypi/xontrib-makefile-complete/)
[![Downloads](https://img.shields.io/pypi/dm/xontrib-makefile-complete?logo=pypi&label=Downloads&color=%2373DC8C)](https://pypi.org/project/xontrib-makefile-complete/)

This provides tab completion for make in xonsh.

## Installation

To install use xpip:

```sh
xpip install xontrib-makefile-complete
```

or

```sh
xpip install -U git+https://codeberg.org.com/taconi/xontrib-makefile-complete
```

## Usage

```sh
xontrib load makefile_complete
 ```
